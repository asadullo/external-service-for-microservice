import * as Express from 'express'
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { AmqpModule } from './modules/AmqpModule';

export interface IAppOptions {
  host: string,
  port: number,
  mongoUrl: string
}

export class Application {

  constructor(options: IAppOptions) {
    this.port = options.port;
    this.mongoUrl = options.mongoUrl
}

  public express;
  public port: number;
  public mongoUrl: string;
  public server;

  async start() {
    try{
      await this.startExpres();
      await this.startAmqp();
    }
    catch(error) {
      console.log(`== Application start ERROR ==\n`, { message: error.message, stack: error.stack})
    }
  }
  async startExpres() {
    let corsOptions = {
      "origin": "*",
      "methods": "GET,PUT,POST,DELETE,OPTIONS",
      "allowedHeaders": ['Content-Type', 'Authorization', 'Lang'],
      "preflightContinue": false,
      "optionsSuccessStatus": 204
  };
  this.express = Express();
  this.express.use(bodyParser.json({ limit: '50mb' }));
  this.express.use(cors(corsOptions));
  this.server = this.express.listen(this.port, console.log(`Server listening on port ${this.port}...`));
  }
  
  async startAmqp() {
    try{
      const amqpInstance = AmqpModule.getInstance()
      await amqpInstance.connect();
      console.log(`Successfully connected to AMQP server on ${process.env.AMQP_HOST}:${process.env.AMQP_PORT}`)
    }
    catch(error) {
      console.log(`AMQP connection error \n`, { message: error.message, stack: error.stack });
    }
  }
}