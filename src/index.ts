import { Application, IAppOptions } from "./Application";
import * as dotenv from 'dotenv';

dotenv.config();

let options: IAppOptions = {
  host: process.env.APP_HOST,
  port: +process.env.APP_PORT,
  mongoUrl: process.env.LOGGER_URL
}
const app = new Application(options);
app.start();