import * as AmqpLib from 'amqplib';

export class AmqpModule {
  private static instance: AmqpModule;
  private connection: AmqpLib.Connection;
  private channel: AmqpLib.Channel;
  private static topic = {
    send: 'external_to_mvc_core',
    listen: 'mvc_core_to_external'
  }

  private constructor() {

  }

  public static getInstance() {
    if(!AmqpModule.instance) {
      AmqpModule.instance = new AmqpModule();
    }
    return AmqpModule.instance;
  }

  async connect() {
    try{
      this.connection = await AmqpLib.connect(`amqp://${process.env.AMQP_HOST}`);
      this.channel = await this.connection.createChannel();
      this.channel.assertQueue(AmqpModule.topic.send);
      this.channel.assertQueue(AmqpModule.topic.listen);

      this.channel.consume(AmqpModule.topic.listen, async msg => {
        console.log('Msg from Core: ', msg.content.toString());
        const res = JSON.parse(msg.content.toString());
        switch(res.method) {
          case 'something about log':
        }
      }, {
        noAck: true
      })
    }
    catch(error) {
      console.log(`Amqp connection error on external-service \n`, { 
        message: error.message,
        stack: error.stack
      });
      
    }
  }

  async send(method: string, params: any) {
    this.channel.sendToQueue(AmqpModule.topic.send, Buffer.from(JSON.stringify({ method, params})));
  }
}